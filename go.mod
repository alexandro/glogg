module gitlab.com/alexandro/glogg

replace gitlab.com/alexandro/glogg => ./

go 1.15

require (
	cloud.google.com/go/logging v1.0.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.1
)
