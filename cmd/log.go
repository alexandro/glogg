/*
Copyright © 2020 Alejandro Quisbert <alexandro@autistici.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"context"
	"log"

	"github.com/spf13/viper"
	"github.com/spf13/cobra"
	"cloud.google.com/go/logging"
	

	// "gitlab.com/alexandro/glogg/src/"
)


// logCmd represents the log command
var logCmd = &cobra.Command{
	Use:   "log",
	Short: "Upload single log line to Google Logging",
	Long: `Upload a single text type log on Google Logging.

		-s Severity level should be set. Otherwise it is DEFAULT.
			https://cloud.google.com/logging/docs/reference/v2/rest/v2/LogEntry#logseverity
	`,
	Run: func(cmd *cobra.Command, args []string) {
		message, _:= cmd.Flags().GetString("message")
		severity, _:= cmd.Flags().GetString("severity")

		//Read conf file
		projectID := viper.Get("project_id").(string)

		ctx := context.Background()
		client, err := logging.NewClient(ctx, projectID)                                                                                                                                                                                                                                                                       
        if err != nil {                                                                                                                                                                                                                                                                                                        
                log.Fatalf("Failed to create client: %v", err)                                                                                                                                                                                                                                                                 
		}

		logName := viper.Get("log_name").(string)
		
		logger := client.Logger(logName) 

		// Adds an entry to the log buffer.                                                                                                                                                                                                                                                                                    
		logger.Log(logging.Entry{ Payload: message, Severity: logging.ParseSeverity(severity) })
		if err := client.Close(); err != nil {                                                                                                                                                                                                                                                                         
			log.Fatalf("Failed to close client: %v", err)                                                                                                                                                                                                                                                                  
		}                                                                                                                                                                                                                                                                                                                      
																																																																															
		fmt.Printf("Logged: %v\n", message)
	},
}

func init() {
	var Message string
	var Severity string

	rootCmd.AddCommand(logCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// logCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// logCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	logCmd.Flags().StringVarP(&Message, "message", "m", "", "Message to send")
	logCmd.Flags().StringVarP(&Severity, "severity", "s", "", "Severity of the event described in a log entry")
	logCmd.MarkFlagRequired("message")
}
