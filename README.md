# GloGG

GLogG
Google Logging command line 
Small and agnostic Google Logging Client command line implementation written in Go


### Local build
You do need to install a Go on your local computer. Follow the official [documentation](https://golang.org/doc/install)
Alternatively you can built it via a Docker container. https://hub.docker.com/_/golang

```bash
go mod init glogg
go install glogg

## Optional: Add local $PATH
PATH="$PATH:$HOME/go/bin"

# More information
glogg --help
glogg log --help
```

# How to install (Live server)

  - Download/compile `glogg` binary file and store it in `/usr/bin/glogg`
  - Set your Google Service Account credential file. It should allow you write logs.
    ```bash
        export GOOGLE_APPLICATION_CREDENTIALS="/my/credentials/directory/log-writer.json"
    ```
  - Copy and edit `.glogg.yaml` configuration file and set in your $HOME directory

  - Write your first log!
    ```bash
    glogg log -m "foo" -s "DEBUG"
    ```